package com.calebmilligan.codeprocessor.io.xml;

import com.calebmilligan.codeprocessor.Code10Context;
import com.calebmilligan.codeprocessor.io.Code10IO;
import com.calebmilligan.codeprocessor.io.IOSerializer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Caleb Milligan
 * Created on 2/23/2019.
 */
@IOSerializer(value = "XML", fileType = ".xml")
public class XmlIO extends Code10IO {
	@Override
	public void write(Code10Context code10, OutputStream out) throws IOException {
		/*
		try {
			XMLStreamWriter writer = XMLStreamWriterFactory.create(out, "UTF-8");
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement("items");
			for (Item item : items) {
				writer.writeStartElement("item");
				writer.writeAttribute("type", String.valueOf(item.getType().getIdentifier()));
				writer.writeAttribute("serial_code", item.getSerialCode());
				writer.writeAttribute("refurbished", Boolean.toString(item.isRefurbished()));
				writer.writeAttribute("recycled", Boolean.toString(item.isRecycled()));
				writer.writeAttribute("data_destruction_required", Boolean.toString(item.isDataDestructionRequired()));
				writer.writeAttribute("hard_drive_present", Boolean.toString(item.isHardDrivePresent()));
				writer.writeAttribute("hard_drive_wiped", Boolean.toString(item.isHardDriveWiped()));
				writer.writeAttribute("hard_drive_recycled", Boolean.toString(item.isHardDriveRecycled()));
				writer.writeEndElement();
			}
			writer.writeEndElement();
			writer.writeEndDocument();
		}
		catch (XMLStreamException e) {
			throw new IOException(e);
		}
		*/
	}

	@Override
	public Code10Context read(InputStream in) throws IOException {
		return null;
	}
}
