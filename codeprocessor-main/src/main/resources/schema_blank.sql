CREATE TABLE IF NOT EXISTS "employees" (
	"id"            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"first_name"    TEXT NOT NULL,
	"last_name"     TEXT NOT NULL,
	UNIQUE("first_name" COLLATE NOCASE, "last_name" COLLATE NOCASE) ON CONFLICT ROLLBACK
);

CREATE TABLE IF NOT EXISTS "customers" (
    "id"                INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name"              TEXT NOT NULL,
    "correspondent"     TEXT NOT NULL,
    "contact_info"      TEXT NOT NULL,
    "address_line_1"    TEXT,
    "address_line_2"    TEXT,
    "address_line_3"    TEXT,
    UNIQUE("name" COLLATE NOCASE) ON CONFLICT REPLACE
);

CREATE TABLE IF NOT EXISTS "shipments" (
    "id"            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date"          CHARACTER(8) NOT NULL,
    "bol_number"    TEXT,
    "customer_id"   INTEGER,
    "employee_id"   INTEGER,
    FOREIGN KEY("customer_id") REFERENCES "customers"("id") ON DELETE RESTRICT,
    FOREIGN KEY("employee_id") REFERENCES "employees"("id") ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS "items" (
    "id"                        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "shipment_id"               INTEGER NOT NULL,
    "type"                      CHARACTER(1) NOT NULL,
    "serial_code"               TEXT NOT NULL,
    "refurbished"               BOOLEAN NOT NULL DEFAULT 0,
    "recycled"                  BOOLEAN NOT NULL DEFAULT 1,
    "data_destruction_required" BOOLEAN NOT NULL DEFAULT 0,
    "hd_present"                BOOLEAN NOT NULL DEFAULT 0,
    "hd_wiped"                  BOOLEAN NOT NULL DEFAULT 0,
    "hd_recycled"               BOOLEAN NOT NULL DEFAULT 0,
    FOREIGN KEY("shipment_id") REFERENCES "shipments"("id") ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS "configuration" (
    "key"   TEXT NOT NULL PRIMARY KEY,
    "value" TEXT DEFAULT NULL
);