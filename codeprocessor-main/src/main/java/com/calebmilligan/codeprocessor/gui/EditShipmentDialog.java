package com.calebmilligan.codeprocessor.gui;

import com.calebmilligan.codeprocessor.Context;
import com.calebmilligan.codeprocessor.gui.editor.ComboBoxCellEditor;
import com.calebmilligan.codeprocessor.gui.fragment.ItemTableRow;
import com.calebmilligan.codeprocessor.gui.model.BackedTableModel;
import com.calebmilligan.codeprocessor.gui.renderer.ButtonCellColumn;
import com.calebmilligan.codeprocessor.gui.renderer.ComboBoxCellRenderer;
import com.calebmilligan.codeprocessor.struct.Customer;
import com.calebmilligan.codeprocessor.struct.Employee;
import com.calebmilligan.codeprocessor.struct.Item;
import com.calebmilligan.codeprocessor.struct.Shipment;
import com.calebmilligan.codeprocessor.util.ScannerInput;
import com.calebmilligan.codeprocessor.util.logging.Loggers;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.*;

public class EditShipmentDialog extends JDialog {
	private final Context context;
	private final Shipment shipment;
	private JPanel content_pane;
	private JButton button_ok;
	private JButton button_cancel;
	private JComboBox<Customer> combo_box_customer;
	private JComboBox<Employee> combo_box_employee;
	private JDatePickerImpl date_picker_date;
	private JTextField text_field_bol_number;
	private JButton button_add_item;
	private JTable table_items;
	private JComboBox<Item.Type> combo_box_item_type;
	private JTextField text_field_item_serial_code;
	private JRadioButton radio_button_item_recycle;
	private JRadioButton radio_button_item_refurbish;
	private JCheckBox check_box_dd_req;
	private JCheckBox check_box_hd_present;
	private JCheckBox check_box_hd_wiped;
	private JCheckBox check_box_hd_recycled;
	private UtilDateModel date_model;
	private Item current_item = null;
	private final ScannerInput.Listener input_listener = new ScannerInput.Listener() {
		@Override
		public boolean onMessageReceived(String message) {
			switch (message.toLowerCase()) {
				case "{recycle}":
					radio_button_item_recycle.doClick();
					return false;
				case "{refurbish}":
					radio_button_item_refurbish.doClick();
					return false;
				case "{dd_req}":
					check_box_dd_req.doClick();
					return false;
				case "{hd_pres}":
					check_box_hd_present.doClick();
					return false;
				case "{hd_wipe}":
					check_box_hd_wiped.doClick();
					return false;
				case "{hd_recycle}":
					check_box_hd_recycled.doClick();
					return false;
				case "{rm_last}":
					if (model.getRowCount() == 0) {
						return false;
					}
					try (Connection connection = context.getConnection()) {
						model.getRows().remove(model.getRowCount() - 1).getItem().delete(connection);
						table_items.updateUI();
					}
					catch (SQLException e) {
						e.printStackTrace();
						Loggers.USER.severe("An error occurred while deleting item");
					}
					return false;
			}
			if (message.toLowerCase().matches("\\{.}")) {
				char identifier = Character.toLowerCase(message.charAt(1));
				for (Item.Type type : Item.Type.values()) {
					if (Character.toLowerCase(type.getIdentifier()) == identifier) {
						if (!text_field_item_serial_code.getText().trim().isEmpty()) {
							button_add_item.doClick();
						}
						combo_box_item_type.setSelectedItem(type);
						return false;
					}
				}
			}
			text_field_item_serial_code.setText(message);
			return false;
		}
	};
	
	private final BackedTableModel<ItemTableRow> model;
	
	
	public EditShipmentDialog(Context context, JFrame parent, Shipment shipment) {
		super(parent);
		this.context = context;
		this.shipment = shipment;
		
		BackedTableModel<ItemTableRow> temp_model = null;
		
		$$$setupUI$$$();
		try {
			temp_model = new BackedTableModel<>(new LinkedList<>(), ItemTableRow.class);
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		model = temp_model;
		table_items.setModel(model);
		new ButtonCellColumn(table_items, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable) e.getSource();
				int row = Integer.valueOf(e.getActionCommand());
				@SuppressWarnings("unchecked")
				BackedTableModel<ItemTableRow> model = (BackedTableModel<ItemTableRow>) table.getModel();
				try (Connection connection = context.getConnection()) {
					Item item = model.getRows().remove(row).getItem();
					item.delete(connection);
					shipment.getItems().remove(item);
				}
				catch (SQLException e1) {
					e1.printStackTrace();
					Loggers.USER.severe("An error occurred while deleting item");
				}
				model.fireTableRowsDeleted(row, row);
			}
		}, 0);
		
		setContentPane(content_pane);
		
		button_ok.addActionListener(e -> onOK());
		
		button_cancel.addActionListener(e -> onCancel());
		
		combo_box_item_type.addActionListener(e -> button_add_item.setEnabled(validateInput()));
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				onCancel();
			}
		});
		
		content_pane.registerKeyboardAction(e -> table_items.getSelectionModel().clearSelection(),
				KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		try (Connection connection = context.getConnection()) {
			try {
				List<Employee> employees = Employee.getAllEmployees(connection);
				Employee[] arr = new Employee[employees.size()];
				employees.toArray(arr);
				combo_box_employee.setModel(new DefaultComboBoxModel<>(arr));
			}
			catch (SQLException e) {
				e.printStackTrace();
				Loggers.USER.severe("An error occurred while loading employee list");
				dispose();
				return;
			}
			try {
				List<Customer> customers = Customer.getAllCustomers(connection);
				Customer[] arr = new Customer[customers.size()];
				customers.toArray(arr);
				combo_box_customer.setModel(new DefaultComboBoxModel<>(arr));
			}
			catch (SQLException e) {
				e.printStackTrace();
				Loggers.USER.severe("An error occurred while loading customer list");
				dispose();
				return;
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			Loggers.USER.severe("An error occurred while establishing a database connection");
			dispose();
			return;
		}
		
		combo_box_employee.addActionListener(e -> shipment.setEmployee((Employee) combo_box_employee.getSelectedItem()));
		combo_box_customer.addActionListener(e -> shipment.setCustomer((Customer) combo_box_customer.getSelectedItem()));
		date_picker_date.addActionListener(e -> shipment.setDate(date_model.getValue()));
		text_field_bol_number.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				shipment.setBolNumber(text_field_bol_number.getText());
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				shipment.setBolNumber(text_field_bol_number.getText());
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				shipment.setBolNumber(text_field_bol_number.getText());
			}
		});
		table_items.setDefaultRenderer(Item.Type.class, new ComboBoxCellRenderer<Item.Type>());
		table_items.setDefaultEditor(Item.Type.class, new ComboBoxCellEditor<>(Item.Type.values()));
		table_items.getSelectionModel().addListSelectionListener(e -> {
			if (e.getValueIsAdjusting()) {
				return;
			}
			
			int index = table_items.getSelectedRow();
			if (index < 0) {
				resetItemInputFields();
				return;
			}
			Item item = model.getRows().get(index).getItem();
			current_item = item;
			combo_box_item_type.setSelectedItem(item.getType());
			text_field_item_serial_code.setText(item.getSerialCode());
			radio_button_item_recycle.setSelected(item.isRecycled());
			radio_button_item_refurbish.setSelected(item.isRefurbished());
			check_box_dd_req.setSelected(item.isDataDestructionRequired());
			check_box_hd_present.setSelected(item.isHardDrivePresent());
			check_box_hd_wiped.setSelected(item.isHardDriveWiped());
			check_box_hd_recycled.setSelected(item.isHardDriveRecycled());
		});
		
		refreshItems();
		combo_box_customer.setSelectedItem(shipment.getCustomer());
		combo_box_employee.setSelectedItem(shipment.getEmployee());
		date_model.setValue(shipment.getDate());
		text_field_bol_number.setText(shipment.getBoLNumber().orElse(null));
		
		context.getInput().addListener(input_listener);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				context.getInput().removeListener(input_listener);
			}
		});
		
		pack();
		setMinimumSize(getSize());
		setLocationByPlatform(true);
		button_add_item.addActionListener(e -> {
			if (current_item == null) {
				Item item = new Item(
						-1,
						(Item.Type) combo_box_item_type.getSelectedItem(),
						text_field_item_serial_code.getText().trim().toLowerCase(),
						radio_button_item_refurbish.isSelected(),
						radio_button_item_recycle.isSelected(),
						check_box_dd_req.isSelected(),
						check_box_hd_present.isSelected(),
						check_box_hd_wiped.isSelected(),
						check_box_hd_recycled.isSelected()
				);
				shipment.getItems().add(item);
				model.getRows().add(new ItemTableRow(item));
			}
			else {
				current_item.setType((Item.Type) combo_box_item_type.getSelectedItem());
				current_item.setSerialCode(text_field_item_serial_code.getText().trim().toLowerCase());
				current_item.setRefurbished(radio_button_item_refurbish.isSelected());
				current_item.setRecycled(radio_button_item_recycle.isSelected());
				current_item.setDataDestructionRequired(check_box_dd_req.isSelected());
				current_item.setHardDrivePresent(check_box_hd_present.isSelected());
				current_item.setHardDriveWiped(check_box_hd_wiped.isSelected());
				current_item.setHardDriveRecycled(check_box_hd_recycled.isSelected());
			}
			resetItemInputFields();
			table_items.updateUI();
		});
		text_field_item_serial_code.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				check();
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				check();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				check();
			}
			
			private void check() {
				button_add_item.setEnabled(validateInput());
			}
		});
	}
	
	private boolean validateInput() {
		return !text_field_item_serial_code.getText().trim().isEmpty() && combo_box_item_type.getSelectedItem() != null;
	}
	
	private void resetItemInputFields() {
		combo_box_item_type.setSelectedItem(null);
		text_field_item_serial_code.setText(null);
		radio_button_item_recycle.setSelected(true);
		radio_button_item_refurbish.setSelected(false);
		check_box_dd_req.setSelected(false);
		check_box_hd_present.setSelected(false);
		check_box_hd_wiped.setSelected(false);
		check_box_hd_recycled.setSelected(false);
		current_item = null;
		table_items.clearSelection();
	}
	
	private void refreshItems() {
		List<Item> items = shipment.getItems();
		List<ItemTableRow> rows = new ArrayList<>(items.size());
		for (Item item : items) {
			rows.add(new ItemTableRow(item));
		}
		
		model.getRows().clear();
		model.getRows().addAll(rows);
		
		table_items.updateUI();
	}
	
	private void onOK() {
		try (Connection connection = context.getConnection()) {
			final SaveLoadIndicatorDialog indicator = new SaveLoadIndicatorDialog();
			indicator.setVisible(true);
			shipment.save(connection);
			indicator.dispose();
			dispose();
		}
		catch (SQLException e) {
			e.printStackTrace();
			Loggers.USER.severe("An error occurred while saving shipment");
		}
	}
	
	private void onCancel() {
		int result = JOptionPane.showConfirmDialog(EditShipmentDialog.this, "Exit without saving?", "Done Editing", JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {
			dispose();
		}
	}
	
	private void createUIComponents() {
		ResourceBundle bundle = ResourceBundle.getBundle("org.jdatepicker.i18n.Text", Locale.US);
		Properties properties = new Properties();
		for (String key : bundle.keySet()) {
			properties.put(key, bundle.getString(key));
		}
		date_model = new UtilDateModel();
		JDatePanelImpl date_panel = new JDatePanelImpl(date_model, properties);
		date_picker_date = new JDatePickerImpl(date_panel, new DateComponentFormatter());
		Item.Type[] types = new Item.Type[Item.Type.values().length + 1];
		System.arraycopy(Item.Type.values(), 0, types, 1, types.length - 1);
		combo_box_item_type = new JComboBox<>(types);
	}
	
	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		content_pane = new JPanel();
		content_pane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		content_pane.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
		panel1.add(panel2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		button_ok = new JButton();
		button_ok.setText("OK");
		panel2.add(button_ok, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		button_cancel = new JButton();
		button_cancel.setText("Cancel");
		panel2.add(button_cancel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		content_pane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel3.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Customer");
		panel4.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		combo_box_customer = new JComboBox();
		panel4.add(combo_box_customer, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Employee");
		panel4.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		combo_box_employee = new JComboBox();
		panel4.add(combo_box_employee, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label3 = new JLabel();
		label3.setText("Date");
		panel4.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel4.add(date_picker_date, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setText("BoL #");
		panel4.add(label4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		text_field_bol_number = new JTextField();
		panel4.add(text_field_bol_number, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel3.add(panel5, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		final JPanel panel6 = new JPanel();
		panel6.setLayout(new GridLayoutManager(1, 5, new Insets(0, 0, 0, 0), -1, -1));
		panel5.add(panel6, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		button_add_item = new JButton();
		button_add_item.setEnabled(false);
		button_add_item.setText("Submit");
		panel6.add(button_add_item, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel7 = new JPanel();
		panel7.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
		panel6.add(panel7, new GridConstraints(0, 0, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		final JPanel panel8 = new JPanel();
		panel8.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel7.add(panel8, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		check_box_dd_req = new JCheckBox();
		check_box_dd_req.setText("Data Destruction Required");
		panel8.add(check_box_dd_req, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		check_box_hd_present = new JCheckBox();
		check_box_hd_present.setText("Hard Drive Present");
		panel8.add(check_box_hd_present, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		check_box_hd_wiped = new JCheckBox();
		check_box_hd_wiped.setText("Hard Drive Wiped");
		panel8.add(check_box_hd_wiped, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		check_box_hd_recycled = new JCheckBox();
		check_box_hd_recycled.setText("Hard Drive Recycled");
		panel8.add(check_box_hd_recycled, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel9 = new JPanel();
		panel9.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel7.add(panel9, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		radio_button_item_recycle = new JRadioButton();
		radio_button_item_recycle.setSelected(true);
		radio_button_item_recycle.setText("Recycle");
		panel9.add(radio_button_item_recycle, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		radio_button_item_refurbish = new JRadioButton();
		radio_button_item_refurbish.setText("Refurbish");
		panel9.add(radio_button_item_refurbish, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel10 = new JPanel();
		panel10.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel7.add(panel10, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel10.add(combo_box_item_type, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label5 = new JLabel();
		label5.setText("Type");
		panel10.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel11 = new JPanel();
		panel11.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel7.add(panel11, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		text_field_item_serial_code = new JTextField();
		panel11.add(text_field_item_serial_code, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		final JLabel label6 = new JLabel();
		label6.setText("Serial Code");
		panel11.add(label6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setAutoscrolls(true);
		panel5.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		table_items = new JTable();
		table_items.setAutoscrolls(true);
		scrollPane1.setViewportView(table_items);
		ButtonGroup buttonGroup;
		buttonGroup = new ButtonGroup();
		buttonGroup.add(radio_button_item_recycle);
		buttonGroup.add(radio_button_item_refurbish);
	}
	
	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return content_pane;
	}
	
}
