package com.calebmilligan.codeprocessor.gui.renderer;

import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public interface ComponentCellRenderer {
	Border no_focus_border = new EmptyBorder(1, 1, 1, 1);
}
