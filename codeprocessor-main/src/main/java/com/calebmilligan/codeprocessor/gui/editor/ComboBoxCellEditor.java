package com.calebmilligan.codeprocessor.gui.editor;

import javax.swing.*;

public class ComboBoxCellEditor<E> extends DefaultCellEditor {
	public ComboBoxCellEditor(E[] values) {
		super(new JComboBox<>(values));
	}
}
