package com.calebmilligan.codeprocessor.gui.renderer;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ButtonCellColumn extends AbstractCellEditor implements TableCellRenderer, TableCellEditor, ActionListener,
		MouseListener {
	private JTable table;
	private Action action;
	private int mnemonic;
	private Border original_border;
	private Border focus_border;
	
	private JButton render_button;
	private JButton edit_button;
	private Object editor_value;
	private boolean button_column_editor;
	
	/**
	 * Create the ButtonColumn to be used as a renderer and editor. The
	 * renderer and editor will automatically be installed on the TableColumn
	 * of the specified column.
	 *
	 * @param table  the table containing the button renderer/editor
	 * @param action the Action to be invoked when the button is invoked
	 * @param column the column to which the button renderer/editor is added
	 */
	public ButtonCellColumn(JTable table, Action action, int column) {
		this.table = table;
		this.action = action;
		
		render_button = new JButton();
		edit_button = new JButton();
		edit_button.setFocusPainted(false);
		edit_button.addActionListener(this);
		original_border = edit_button.getBorder();
		setFocusBorder(new LineBorder(Color.BLUE));
		
		TableColumnModel column_model = table.getColumnModel();
		column_model.getColumn(column).setCellRenderer(this);
		column_model.getColumn(column).setCellEditor(this);
		table.addMouseListener(this);
	}
	
	
	/**
	 * Get foreground color of the button when the cell has focus
	 *
	 * @return the foreground color
	 */
	public Border getFocusBorder() {
		return focus_border;
	}
	
	/**
	 * The foreground color of the button when the cell has focus
	 *
	 * @param focus_border the foreground color
	 */
	public void setFocusBorder(Border focus_border) {
		this.focus_border = focus_border;
		edit_button.setBorder(focus_border);
	}
	
	public int getMnemonic() {
		return mnemonic;
	}
	
	/**
	 * The mnemonic to activate the button when the cell has focus
	 *
	 * @param mnemonic the mnemonic
	 */
	public void setMnemonic(int mnemonic) {
		this.mnemonic = mnemonic;
		render_button.setMnemonic(mnemonic);
		edit_button.setMnemonic(mnemonic);
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		if (value == null) {
			edit_button.setText("");
			edit_button.setIcon(null);
		}
		else if (value instanceof Icon) {
			edit_button.setText("");
			edit_button.setIcon((Icon) value);
		}
		else {
			edit_button.setText(value.toString());
			edit_button.setIcon(null);
		}
		
		this.editor_value = value;
		return edit_button;
	}
	
	@Override
	public Object getCellEditorValue() {
		return editor_value;
	}
	
	public Component getTableCellRendererComponent(
			JTable table, Object value, boolean selected, boolean focused, int row, int column) {
		if (selected) {
			render_button.setForeground(table.getSelectionForeground());
			render_button.setBackground(table.getSelectionBackground());
		}
		else {
			render_button.setForeground(table.getForeground());
			render_button.setBackground(UIManager.getColor("Button.background"));
		}
		
		if (focused) {
			render_button.setBorder(focus_border);
		}
		else {
			render_button.setBorder(original_border);
		}

//		render_button.setText( (value == null) ? "" : value.toString() );
		if (value == null) {
			render_button.setText("");
			render_button.setIcon(null);
		}
		else if (value instanceof Icon) {
			render_button.setText("");
			render_button.setIcon((Icon) value);
		}
		else {
			render_button.setText(value.toString());
			render_button.setIcon(null);
		}
		
		return render_button;
	}
	
	public void actionPerformed(ActionEvent e) {
		int row = table.convertRowIndexToModel(table.getEditingRow());
		fireEditingStopped();
		
		ActionEvent event = new ActionEvent(
				table,
				ActionEvent.ACTION_PERFORMED,
				String.valueOf(row)
		);
		action.actionPerformed(event);
	}
	
	public void mousePressed(MouseEvent e) {
		if (table.isEditing() && table.getCellEditor() == this) {
			button_column_editor = true;
		}
	}
	
	public void mouseReleased(MouseEvent e) {
		if (button_column_editor && table.isEditing()) {
			table.getCellEditor().stopCellEditing();
		}
		
		button_column_editor = false;
	}
	
	public void mouseClicked(MouseEvent e) {
	}
	
	public void mouseEntered(MouseEvent e) {
	}
	
	public void mouseExited(MouseEvent e) {
	}
}