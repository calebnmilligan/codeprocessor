package com.calebmilligan.codeprocessor.gui.fragment;

import com.calebmilligan.codeprocessor.gui.model.BackedTableModel;
import com.calebmilligan.codeprocessor.struct.Shipment;

import java.util.Date;

public class ShipmentTableRow implements BackedTableModel.TableRow {
	private static final Class[] column_classes = {
			Date.class,
			String.class,
			String.class,
			String.class
	};
	
	private static final String[] column_names = {
			"Date",
			"Customer",
			"Employee",
			"BoL #"
	};
	
	private final Shipment shipment;
	
	private ShipmentTableRow() {
		this(null);
	}
	
	public ShipmentTableRow(Shipment shipment) {
		this.shipment = shipment;
	}
	
	@Override
	public int getColumnCount() {
		return 4;
	}
	
	@Override
	public Object getColumnValue(int column) {
		switch (column) {
			case 0:
				return shipment.getDate();
			case 1:
				return shipment.getCustomer() == null ? null : shipment.getCustomer().getName();
			case 2:
				return shipment.getEmployee() == null ? null : shipment.getEmployee().getFormattedName();
			case 3:
				return shipment.getBoLNumber().orElse(null);
			default:
				throw new IndexOutOfBoundsException();
			
		}
	}
	
	@Override
	public void setColumnValue(int column, Object value) {
	
	}
	
	@Override
	public Class[] getColumnClasses() {
		return column_classes;
	}
	
	@Override
	public String[] getColumnNames() {
		return column_names;
	}
	
	@Override
	public boolean isColumnEditable(int column) {
		return false;
	}
	
	public Shipment getShipment() {
		return shipment;
	}
}