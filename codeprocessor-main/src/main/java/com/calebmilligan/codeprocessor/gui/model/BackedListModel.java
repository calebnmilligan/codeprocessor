package com.calebmilligan.codeprocessor.gui.model;

import javax.swing.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class BackedListModel<E> extends AbstractListModel<E> {
	private final List<E> list;
	
	public BackedListModel(List<E> list) {
		this.list = new FiringList(list);
	}
	
	public List<E> getList() {
		return list;
	}
	
	@Override
	public int getSize() {
		return list.size();
	}
	
	@Override
	public E getElementAt(int index) throws IndexOutOfBoundsException {
		return list.get(index);
	}
	
	class FiringList implements List<E> {
		private final List<E> wrapped;
		
		public FiringList(List<E> wrapped) {
			this.wrapped = wrapped;
		}
		
		@Override
		public int size() {
			return wrapped.size();
		}
		
		@Override
		public boolean isEmpty() {
			return wrapped.isEmpty();
		}
		
		@Override
		public boolean contains(Object o) {
			return wrapped.contains(o);
		}
		
		@Override
		public Iterator<E> iterator() {
			return wrapped.iterator();
		}
		
		@Override
		public Object[] toArray() {
			return wrapped.toArray();
		}
		
		@Override
		public <T> T[] toArray(T[] a) {
			return wrapped.toArray(a);
		}
		
		@Override
		public boolean add(E element) {
			int index = wrapped.size();
			boolean result = wrapped.add(element);
			fireIntervalAdded(BackedListModel.this, index, index);
			return result;
		}
		
		@Override
		public boolean remove(Object element) {
			int index = indexOf(element);
			boolean result = wrapped.remove(element);
			fireIntervalRemoved(BackedListModel.this, index, index);
			return result;
		}
		
		@Override
		public boolean containsAll(Collection<?> c) {
			return wrapped.containsAll(c);
		}
		
		@Override
		public boolean addAll(Collection<? extends E> c) {
			throw new UnsupportedOperationException(); // TODO
		}
		
		@Override
		public boolean addAll(int index, Collection<? extends E> c) {
			throw new UnsupportedOperationException(); // TODO
		}
		
		@Override
		public boolean removeAll(Collection<?> c) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public boolean retainAll(Collection<?> c) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public void clear() {
			int index = wrapped.size() - 1;
			wrapped.clear();
			fireIntervalRemoved(BackedListModel.this, 0, index);
		}
		
		@Override
		public E get(int index) {
			return wrapped.get(index);
		}
		
		@Override
		public E set(int index, E element) {
			E result = wrapped.set(index, element);
			fireContentsChanged(BackedListModel.this, index, index);
			return result;
		}
		
		@Override
		public void add(int index, E element) {
			wrapped.add(index, element);
			fireIntervalAdded(BackedListModel.this, index, index);
		}
		
		@Override
		public E remove(int index) {
			E result = wrapped.remove(index);
			fireIntervalRemoved(BackedListModel.this, index, index);
			return result;
		}
		
		@Override
		public int indexOf(Object o) {
			return wrapped.indexOf(o);
		}
		
		@Override
		public int lastIndexOf(Object o) {
			return wrapped.lastIndexOf(o);
		}
		
		@Override
		public ListIterator<E> listIterator() {
			return wrapped.listIterator();
		}
		
		@Override
		public ListIterator<E> listIterator(int index) {
			return wrapped.listIterator(index);
		}
		
		@Override
		public List<E> subList(int from, int to) {
			return new FiringList(wrapped.subList(from, to));
		}
	}
}
