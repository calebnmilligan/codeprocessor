package com.calebmilligan.codeprocessor.gui.renderer;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ComponentListCellRenderer<E> extends JPanel implements ListCellRenderer<E> {
	public ComponentListCellRenderer() {
		super(new GridBagLayout(), true);
		setOpaque(false);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends E> list, E value, int index, boolean selected, boolean focused) {
		Component component = value instanceof Component ? (Component) value : new JLabel(value == null ? "" : value.toString());
		removeAll();
		add(component, new GridBagConstraints(
				GridBagConstraints.RELATIVE,
				GridBagConstraints.RELATIVE, 1,
				1,
				0,
				0,
				GridBagConstraints.CENTER,
				GridBagConstraints.BOTH,
				new Insets(1, 1, 1, 1),
				0,
				0));
		if (selected) {
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		}
		else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}
		if (focused) {
			setBorder(new LineBorder(list.getSelectionBackground().darker()));
		}
		
		return this;
	}
}
