package com.calebmilligan.codeprocessor.gui.model;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Constructor;
import java.util.List;

/**
 * @author Caleb Milligan
 * Created on 2/27/2019.
 */
public class BackedTableModel<T extends BackedTableModel.TableRow> extends AbstractTableModel {
	private final List<T> rows;
	private final Class[] column_classes;
	private final String[] column_names;
	private final int column_count;
	
	public BackedTableModel(List<T> rows, Class<T> row_class) throws IllegalArgumentException, ReflectiveOperationException {
		this.rows = rows;
		try {
			Constructor<T> ctor = row_class.getDeclaredConstructor();
			ctor.setAccessible(true);
			T instance = ctor.newInstance();
			this.column_count = instance.getColumnCount();
			this.column_classes = instance.getColumnClasses();
			this.column_names = instance.getColumnNames();
		}
		catch (NoSuchMethodException e) {
			throw new IllegalArgumentException("Row class does not implement a no-args constructor");
		}
	}
	
	@Override
	public String getColumnName(int column) {
		return column_names[column];
	}
	
	@Override
	public Class getColumnClass(int column) {
		return column_classes[column];
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return rows.get(row).isColumnEditable(column);
	}
	
	@Override
	public int getRowCount() {
		return rows.size();
	}
	
	@Override
	public int getColumnCount() {
		return column_count;
	}
	
	@Override
	public Object getValueAt(int row, int column) {
		return rows.get(row).getColumnValue(column);
	}
	
	public List<T> getRows() {
		return rows;
	}
	
	public interface TableRow {
		int getColumnCount();
		
		Object getColumnValue(int column);
		
		void setColumnValue(int column, Object value);
		
		Class[] getColumnClasses();
		
		String[] getColumnNames();
		
		boolean isColumnEditable(int column);
	}
}
