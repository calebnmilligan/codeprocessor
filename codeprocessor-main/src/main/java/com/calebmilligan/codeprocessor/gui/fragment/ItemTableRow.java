package com.calebmilligan.codeprocessor.gui.fragment;

import com.calebmilligan.codeprocessor.gui.model.BackedTableModel;
import com.calebmilligan.codeprocessor.struct.Item;

public class ItemTableRow implements BackedTableModel.TableRow {
	private static final Class[] column_classes = {
			String.class,
			String.class,
			String.class,
			String.class,
			String.class,
			String.class,
			String.class,
			String.class,
			String.class
	};
	private static final String[] column_names = {
			"",
			"Type",
			"Serial Code",
			"Refurbished",
			"Recycled",
			"Date Destruction Required",
			"Hard Drive Present",
			"Hard Drive Wiped",
			"Hard Drive Recycled"
	};
	
	private final Item item;
	
	private ItemTableRow() {
		this(null);
	}
	
	public ItemTableRow(Item item) {
		this.item = item;
	}
	
	@Override
	public Object getColumnValue(int column) {
		if (item == null) {
			return null;
		}
		switch (column) {
			case 0:
				return "X";
			case 1:
				return item.getType().getIdentifier();
			case 2:
				return item.getSerialCode();
			case 3:
				return item.isRefurbished() ? "Y" : "N";
			case 4:
				return item.isRecycled() ? "Y" : "N";
			case 5:
				return item.isDataDestructionRequired() ? "Y" : "N";
			case 6:
				return item.isHardDrivePresent() ? "Y" : "N";
			case 7:
				return item.isHardDriveWiped() ? "Y" : "N";
			case 8:
				return item.isHardDriveRecycled() ? "Y" : "N";
			default:
				throw new IndexOutOfBoundsException();
		}
	}
	
	@Override
	public void setColumnValue(int column, Object value) {
	
	}
	
	public Item getItem() {
		return item;
	}
	
	@Override
	public int getColumnCount() {
		return 9;
	}
	
	@Override
	public Class[] getColumnClasses() {
		return column_classes;
	}
	
	@Override
	public String[] getColumnNames() {
		return column_names;
	}
	
	@Override
	public boolean isColumnEditable(int column) {
		return column == 0;
	}
}
