package com.calebmilligan.codeprocessor.gui.renderer;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class ComboBoxCellRenderer<E> extends JComboBox<E> implements TableCellRenderer, ComponentCellRenderer {
	public ComboBoxCellRenderer() {
		super();
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
		if (selected) {
			setForeground(table.getSelectionForeground());
			super.setBackground(table.getSelectionBackground());
		}
		else {
			setForeground(table.getForeground());
			setBackground(table.getBackground());
		}
		
		setSelectedItem(value);
		
		if (focused) {
			setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
		}
		else {
			setBorder(no_focus_border);
		}
		
		return this;
	}
}
