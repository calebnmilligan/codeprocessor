package com.calebmilligan.codeprocessor.util.logging;

import java.util.logging.ConsoleHandler;
import java.util.logging.LogRecord;

/**
 * @author Caleb Milligan
 * Created on 2/25/2019.
 */
class HandyLogHandler extends ConsoleHandler {
	public HandyLogHandler() {
		super();
	}
	
	@Override
	public void publish(LogRecord record) {
		super.publish(record);
	}
}
