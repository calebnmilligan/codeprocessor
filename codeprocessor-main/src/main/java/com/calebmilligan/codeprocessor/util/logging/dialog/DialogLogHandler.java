package com.calebmilligan.codeprocessor.util.logging.dialog;

import javax.swing.*;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class DialogLogHandler extends Handler {
	private boolean closed = false;
	
	@Override
	public void publish(LogRecord record) {
		if (!closed) {
			int type;
			switch (record.getLevel().getName().toLowerCase()) {
				case "severe":
					type = JOptionPane.ERROR_MESSAGE;
					break;
				case "warning":
					type = JOptionPane.WARNING_MESSAGE;
					break;
				case "info":
					type = JOptionPane.INFORMATION_MESSAGE;
					break;
				default:
					type = JOptionPane.PLAIN_MESSAGE;
					break;
				
			}
			JOptionPane.showMessageDialog(
					null,
					record.getMessage(),
					record.getLoggerName(),
					type
			);
		}
	}
	
	@Override
	public void flush() {
	
	}
	
	@Override
	public void close() {
		closed = true;
	}
}
