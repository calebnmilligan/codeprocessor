package com.calebmilligan.codeprocessor.util.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * @author Caleb Milligan
 * Created on 2/25/2019.
 */
class HandyLogFormatter extends Formatter {
	private static final String format = "[%1$s] [%2$s/%3$s] [%4$s]: %5$s\n";
	private static final DateFormat time_format = new SimpleDateFormat("HH:mm:ss");
	
	@Override
	public String format(LogRecord record) {
		String thread_name = "Thread #" + record.getThreadID();
		for (Thread thread : Thread.getAllStackTraces().keySet()) {
			if (thread.getId() == record.getThreadID()) {
				thread_name = thread.getName();
				break;
			}
		}
		return String.format(
				format,
				time_format.format(new Date(record.getMillis())),
				thread_name,
				record.getLevel().getName(),
				record.getLoggerName(),
				record.getMessage()
		);
	}
}
