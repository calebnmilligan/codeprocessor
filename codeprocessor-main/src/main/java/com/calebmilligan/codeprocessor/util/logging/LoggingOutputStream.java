package com.calebmilligan.codeprocessor.util.logging;

import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Caleb Milligan
 * Created on 2/25/2019.
 */
class LoggingOutputStream extends OutputStream {
	private final Logger logger;
	private final Level record_level;
	
	public LoggingOutputStream(Logger logger, Level record_level) {
		this.logger = logger;
		this.record_level = record_level;
	}
	
	@Override
	public void write(int b) {
		logger.log(record_level, Character.toString((char) b));
	}
	
	@Override
	public void write(byte[] buf, int offset, int length) {
		for (String line : new String(buf, offset, length).split("\n")) {
			logger.log(record_level, line);
		}
	}
}
