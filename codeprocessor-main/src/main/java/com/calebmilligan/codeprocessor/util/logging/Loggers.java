package com.calebmilligan.codeprocessor.util.logging;

import com.calebmilligan.codeprocessor.util.logging.dialog.DialogLogHandler;

import java.util.logging.Logger;

/**
 * @author Caleb Milligan
 * Created on 2/26/2019.
 */
public interface Loggers {
	Logger MAIN = Code10Log.getLogger("Code10");
	Logger GUI = Code10Log.getLogger("GUI");
	Logger OUTPUT = Code10Log.getLogger("Output");
	Logger INPUT = Code10Log.getLogger("Input");
	Logger USER = Code10Log.getLogger("User", new DialogLogHandler());
}
