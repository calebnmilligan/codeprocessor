package com.calebmilligan.codeprocessor.util.logging;

import java.io.PrintStream;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Caleb Milligan
 * Created on 2/25/2019.
 */
public class Code10Log {
	private static boolean overridden = false;
	private static PrintStream default_out;
	private static PrintStream default_err;
	
	public static synchronized void overrideSystemStreams() {
		if (overridden) {
			throw new IllegalStateException("Already overridden");
		}
		default_err = System.err;
		default_out = System.out;
		System.setOut(new PrintStream(new LoggingOutputStream(Loggers.MAIN, Level.INFO)));
		System.setErr(new PrintStream(new LoggingOutputStream(Loggers.MAIN, Level.WARNING)));
		overridden = true;
	}
	
	public static synchronized void revertSystemStreams() {
		if (!overridden) {
			throw new IllegalStateException("Not overridden");
		}
		System.setErr(default_err);
		System.setOut(default_out);
		overridden = false;
	}
	
	public static Logger getLogger(String name) {
		Handler handler = new HandyLogHandler();
		handler.setFormatter(new HandyLogFormatter());
		return getLogger(name, handler);
	}
	
	public static Logger getLogger(String name, Handler handler) {
		Logger logger = Logger.getLogger(name);
		if (handler != null) {
			logger.setUseParentHandlers(false);
			logger.addHandler(handler);
		}
		return logger;
	}
}
