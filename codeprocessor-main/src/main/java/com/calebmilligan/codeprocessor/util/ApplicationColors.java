package com.calebmilligan.codeprocessor.util;

import java.awt.*;

public interface ApplicationColors {
	Color PALE_RED = new Color(0xFFCCCC);
	Color PALE_GREEN = new Color(0xCCFFCC);
}
