package com.calebmilligan.codeprocessor.util;

import com.calebmilligan.codeprocessor.Context;
import com.calebmilligan.codeprocessor.util.logging.Loggers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Caleb Milligan
 * Created on 2/27/2019.
 */
public final class ScannerInput {
	private final Object listeners_lock = new Object();
	private final List<Listener> listeners = new ArrayList<>();
	private Pattern format_pattern = Pattern.compile("(.*)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private boolean format_has_suffix = false;
	private final Listener format_listener;
	
	public ScannerInput(final Context context) {
		this.format_listener = message -> {
			int start_index;
			if ((start_index = message.toLowerCase().indexOf("{conf}")) > -1) {
				int end_index = start_index + "{conf}".length();
				String prefix = message.substring(0, start_index);
				String suffix = message.substring(end_index);
				format_pattern = Pattern.compile(Utils.escapeRegex(prefix) + "(.*)" + Utils.escapeRegex(suffix), Pattern.DOTALL);
				format_has_suffix = suffix.length() > 0;
				context.getConfig().set("input.format.pattern", format_pattern.pattern());
				context.getConfig().set("input.format.has_suffix", format_has_suffix);
				try (Connection connection = context.getConnection()) {
					context.getConfig().save(connection);
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
				Loggers.INPUT.info("Input format changed to \"" + Utils.unescapeSpecialChars(format_pattern.pattern()) + "\"");
				Loggers.USER.info("Input format changed successfully");
				return true;
			}
			return false;
		};
	}
	
	public void setFormatPattern(String pattern, boolean has_suffix) {
		this.format_pattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		this.format_has_suffix = has_suffix;
	}
	
	public Pattern getFormatPattern() {
		return format_pattern;
	}
	
	public boolean formatPatternHasSuffix() {
		return format_has_suffix;
	}
	
	public void processMessage(String raw_message) {
		if (format_listener.onMessageReceived(raw_message)) { // Input format detection must happen before modification
			return;
		}
		final String message;
		Matcher matcher = format_pattern.matcher(raw_message);
		if (matcher.find()) {
			message = matcher.group(1);
			if (message.isEmpty()) {
				return;
			}
		}
		else {
			message = raw_message;
		}
		Loggers.INPUT.info("Received message: " + message);
		synchronized (listeners_lock) {
			for (Listener listener : listeners) {
				// Ensure that one listener throwing an error doesn't stop the event train
				try {
					if (listener.onMessageReceived(message)) {
						break;
					}
				}
				catch (Throwable t) {
					t.printStackTrace();
				}
			}
		}
	}
	
	public void addListener(Listener listener) {
		synchronized (listeners_lock) {
			listeners.add(listener);
		}
	}
	
	public void removeListener(Listener listener) {
		synchronized (listeners_lock) {
			listeners.remove(listener);
		}
	}
	
	public interface Listener {
		boolean onMessageReceived(String message);
	}
}
