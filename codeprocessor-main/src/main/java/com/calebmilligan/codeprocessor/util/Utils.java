package com.calebmilligan.codeprocessor.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * @author Caleb Milligan
 * Created on 2/25/2019.
 */
public class Utils {
	public static final SimpleDateFormat ISO_DATE_FORMAT_SHORT = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat ISO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	private static final Pattern SPECIAL_REGEX_CHARS = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]");
	
	public static String escapeRegex(String str) {
		return SPECIAL_REGEX_CHARS.matcher(str).replaceAll("\\\\$0");
	}
	
	private static Image barcode_sheet_image;
	
	public static void printBarcodeSheet() throws IOException, PrinterException {
		PrinterJob job = PrinterJob.getPrinterJob();
		PageFormat format = new PageFormat();
		Paper paper = new Paper();
		paper.setSize(612, 792);
		paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
		format.setPaper(paper);
		format.setOrientation(PageFormat.LANDSCAPE);
		if (barcode_sheet_image == null) {
			BufferedImage image = ImageIO.read(Utils.class.getResourceAsStream("/barcode_sheet.png"));
			barcode_sheet_image = image.getScaledInstance(816, 1056, BufferedImage.SCALE_REPLICATE);
		}
		job.setPrintable((g, format1, page) -> {
			if (page != 0) {
				return Printable.NO_SUCH_PAGE;
			}
			g.drawImage(barcode_sheet_image, 0, 0, barcode_sheet_image.getWidth(null), barcode_sheet_image.getHeight(null), null);
			return Printable.PAGE_EXISTS;
		}, format);
		if (job.printDialog()) {
			job.print();
		}
	}
	
	private static final String[][] special_chars = {
			{"\\", "\\\\"},
			{"\t", "\\t"},
			{"\b", "\\b"},
			{"\n", "\\n"},
			{"\r", "\\r"},
			{"\f", "\\f"},
			{"\'", "\\'"},
			{"\"", "\\\""}
	};
	
	public static String unescapeSpecialChars(CharSequence str) {
		String operating = String.valueOf(str);
		for (String[] special_char : special_chars) {
			operating = operating.replace(special_char[0], special_char[1]);
		}
		return operating;
	}
}
