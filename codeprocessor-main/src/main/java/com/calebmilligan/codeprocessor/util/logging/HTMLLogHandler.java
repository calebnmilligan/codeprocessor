package com.calebmilligan.codeprocessor.util.logging;

import com.calebmilligan.codeprocessor.util.Utils;

import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import javax.swing.text.html.parser.ParserDelegator;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class HTMLLogHandler extends Handler {
	private final HTMLDocument document;
	private final String format = "<tr class=\"%1$s\"><td>%2$s</td><td>%3$s</td></tr>";
	
	public HTMLLogHandler() {
		StyleSheet styles = new StyleSheet();
		styles.addRule(".info {color: #000000;}");
		styles.addRule(".warning {color: #774400;}");
		styles.addRule(".severe {color: #770000};");
		styles.addRule("table {width: 100%;}");
		styles.addRule("table tr td:first-child {white-space: nowrap; border-right: 1px solid #999999;}");
		styles.addRule("table tr td:last-child {word-wrap: break-word; width: 99%}");
		styles.addRule("table tr {border-bottom: 1px solid #999999;}");
		this.document = new HTMLDocument(styles);
		this.document.setParser(new ParserDelegator());
		try {
			this.document.setInnerHTML(this.document.getDefaultRootElement(), "<table id=\"log_table\"></table>");
		}
		catch (BadLocationException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public HTMLDocument getDocument() {
		return document;
	}
	
	@Override
	public void publish(LogRecord record) {
		try {
			document.insertBeforeEnd(document.getElement("log_table"),
					String.format(
							format,
							record.getLevel().getName().toLowerCase(),
							Utils.ISO_DATE_FORMAT.format(new Date(record.getMillis())),
							record.getMessage()
					)
			);
		}
		catch (IOException | BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void flush() {
	
	}
	
	@Override
	public void close() throws SecurityException {
	
	}
}
