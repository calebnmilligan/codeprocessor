package com.calebmilligan.codeprocessor.util;

import com.calebmilligan.codeprocessor.Context;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;

public class ScannerInputBuffer extends KeyAdapter {
	private static final Timer scanner_format_timer = new Timer("ScannerFormatDetection");
	private static final int submit_delay = 500;
	private final Context context;
	private final StringBuilder buffer = new StringBuilder();
	private final Object buffer_lock = new Object();
	private long last_char_entered;
	private ScannerFormatTimerTask current_task;
	
	public ScannerInputBuffer(Context context) {
		this.context = context;
		scanner_format_timer.schedule(current_task = new ScannerFormatTimerTask(), submit_delay, submit_delay);
	}
	
	class ScannerFormatTimerTask extends TimerTask {
		@Override
		public void run() {
			if (System.currentTimeMillis() - last_char_entered < 500 || buffer.length() == 0) {
				return;
			}
			synchronized (buffer_lock) {
				context.getInput().processMessage(buffer.toString());
				buffer.setLength(0);
			}
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		current_task.cancel();
		last_char_entered = System.currentTimeMillis();
		synchronized (buffer_lock) {
			buffer.append(e.getKeyChar());
		}
		synchronized (buffer_lock) {
			if (context.getInput().formatPatternHasSuffix()
					&& context.getInput().getFormatPattern().matcher(buffer).find()) {
				context.getInput().processMessage(buffer.toString());
				buffer.setLength(0);
			}
		}
		scanner_format_timer.schedule(current_task = new ScannerFormatTimerTask(), submit_delay, submit_delay);
	}
}
