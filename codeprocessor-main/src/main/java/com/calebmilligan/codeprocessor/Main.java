package com.calebmilligan.codeprocessor;

import com.calebmilligan.codeprocessor.gui.MainWindow;
import com.calebmilligan.codeprocessor.util.logging.Code10Log;
import com.calebmilligan.codeprocessor.util.logging.Loggers;

import javax.swing.*;
import java.sql.Connection;

/**
 * @author Caleb Milligan
 * Created on 2/22/2019.
 */
public class Main {
	public static void main(String... args) throws Exception {
		Thread.currentThread().setUncaughtExceptionHandler((t, e) -> {
			e.printStackTrace();
			Loggers.USER.severe("An error has occurred. The program will now exit.");
			System.exit(-1);
		});
		// Send system io to the logger
		Code10Log.overrideSystemStreams();
		
		// Disguise ourselves
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException | ClassNotFoundException e) {
			Loggers.GUI.severe("Failed to set look and feel");
		}
		
		Context context = new Context();
		try (Connection connection = context.getConnection()) {
			context.getConfig().load(connection);
		}
		
		if (context.getConfig().isSet("input.format.pattern") && context.getConfig().isSet("input.format.has_suffix")) {
			String format = context.getConfig().getString("input.format.pattern", "(.*)");
			boolean has_suffix = context.getConfig().getBoolean("input.format.has_suffix", false);
			context.getInput().setFormatPattern(format, has_suffix);
		}
		
		new MainWindow(context).setVisible(true);
	}
}
