package com.calebmilligan.codeprocessor.struct;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Caleb Milligan
 * Created on 2/22/2019.
 */
public class Item extends DatabaseEntry {
	private int shipment_id;
	private Type type = null;
	private String serial_code = null;
	private boolean refurbished = false;
	private boolean recycled = true;
	private boolean data_destruction_required = false;
	private boolean hd_present = false;
	private boolean hd_wiped = false;
	private boolean hd_recycled = false;
	private boolean changed = false;
	private transient int hash_code;
	
	public static List<Item> getAllItems(int shipment_id, Connection connection) throws SQLException {
		List<Item> items = new LinkedList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"id\", \"type\", \"serial_code\", " +
				"\"refurbished\", \"recycled\", \"data_destruction_required\", \"hd_present\", \"hd_wiped\", \"hd_recycled\" " +
				"FROM \"items\" WHERE \"shipment_id\"=?")) {
			statement.setInt(1, shipment_id);
			try (ResultSet result = statement.executeQuery()) {
				while (result.next()) {
					Item item = new Item(
							result.getInt(1),
							shipment_id,
							Type.getType(result.getString(2)),
							result.getString(3),
							result.getBoolean(4),
							result.getBoolean(5),
							result.getBoolean(6),
							result.getBoolean(7),
							result.getBoolean(8),
							result.getBoolean(9)
					);
					items.add(item);
				}
			}
		}
		return new ArrayList<>(items);
	}
	
	public Item(int shipment_id) {
		super();
		this.shipment_id = shipment_id;
		this.changed = true;
		calculateHashCode();
	}
	
	public Item(int shipment_id, Type type, String serial_code, boolean refurbished, boolean recycled,
				boolean data_destruction_required, boolean hd_present, boolean hd_wiped, boolean hd_recycled) {
		super();
		this.shipment_id = shipment_id;
		this.type = type;
		this.serial_code = serial_code.trim().toLowerCase(); // Normalize the serial code
		this.refurbished = refurbished;
		this.recycled = recycled;
		this.data_destruction_required = data_destruction_required;
		this.hd_present = hd_present;
		this.hd_wiped = hd_wiped;
		this.hd_recycled = hd_recycled;
		this.changed = true;
		calculateHashCode();
	}
	
	public Item(int id, int shipment_id, Type type, String serial_code, boolean refurbished, boolean recycled,
				boolean data_destruction_required, boolean hd_present, boolean hd_wiped, boolean hd_recycled) {
		super(id);
		this.shipment_id = shipment_id;
		this.type = type;
		this.serial_code = serial_code.trim().toLowerCase(); // Normalize the serial code
		this.refurbished = refurbished;
		this.recycled = recycled;
		this.data_destruction_required = data_destruction_required;
		this.hd_present = hd_present;
		this.hd_wiped = hd_wiped;
		this.hd_recycled = hd_recycled;
		calculateHashCode();
	}
	
	private void calculateHashCode() {
		String s_l = serial_code == null ? null : serial_code.toLowerCase();
		this.hash_code = Objects.hash(this.type, s_l);
	}
	
	@Override
	public void save(Connection connection) throws SQLException {
		if (!changed) {
			return;
		}
		if (id == null) {
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO \"items\" (\"shipment_id\", " +
					"\"type\", \"serial_code\", \"refurbished\", \"recycled\", \"data_destruction_required\", " +
					"\"hd_present\", \"hd_wiped\", \"hd_recycled\") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
				statement.setInt(1, shipment_id);
				statement.setString(2, String.valueOf(type.identifier));
				statement.setString(3, serial_code);
				statement.setBoolean(4, refurbished);
				statement.setBoolean(5, recycled);
				statement.setBoolean(6, data_destruction_required);
				statement.setBoolean(7, hd_present);
				statement.setBoolean(8, hd_wiped);
				statement.setBoolean(9, hd_recycled);
				statement.execute();
			}
			try (PreparedStatement statement = connection.prepareStatement("SELECT last_insert_rowid()")) {
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						this.id = result.getInt(1);
					}
				}
			}
		}
		else {
			try (PreparedStatement statement = connection.prepareStatement("REPLACE INTO \"items\" (\"id\"," +
					"\"shipment_id\", \"type\", \"serial_code\", \"refurbished\", \"recycled\", \"data_destruction_required\", " +
					"\"hd_present\", \"hd_wiped\", \"hd_recycled\") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
				statement.setInt(1, id);
				statement.setInt(2, shipment_id);
				statement.setString(3, String.valueOf(type.identifier));
				statement.setString(4, serial_code);
				statement.setBoolean(5, refurbished);
				statement.setBoolean(6, recycled);
				statement.setBoolean(7, data_destruction_required);
				statement.setBoolean(8, hd_present);
				statement.setBoolean(9, hd_wiped);
				statement.setBoolean(10, hd_recycled);
				statement.execute();
			}
		}
		this.changed = false;
	}
	
	@Override
	public void delete(Connection connection) throws SQLException {
		if (id == null) {
			return;
		}
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM \"items\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			statement.execute();
		}
	}
	
	public boolean hasChanged() {
		return changed;
	}
	
	public int getShipmentId() {
		return shipment_id;
	}
	
	public void setShipmentId(int shipment_id) {
		this.changed |= this.shipment_id != shipment_id;
		this.shipment_id = shipment_id;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.changed |= !Objects.equals(this.type, type);
		this.type = type;
		calculateHashCode();
	}
	
	public String getSerialCode() {
		return serial_code;
	}
	
	public void setSerialCode(String serial_code) {
		this.changed |= !Objects.equals(this.serial_code, serial_code);
		this.serial_code = serial_code;
		calculateHashCode();
	}
	
	public boolean isRefurbished() {
		return refurbished;
	}
	
	public void setRefurbished(boolean refurbished) {
		this.changed |= this.refurbished != refurbished;
		this.refurbished = refurbished;
		this.recycled = !refurbished;
	}
	
	public boolean isRecycled() {
		return recycled;
	}
	
	public void setRecycled(boolean recycled) {
		this.changed |= this.recycled != recycled;
		this.recycled = recycled;
		this.refurbished = !recycled;
	}
	
	public boolean isDataDestructionRequired() {
		return data_destruction_required;
	}
	
	public void setDataDestructionRequired(boolean data_destruction_required) {
		this.changed |= this.data_destruction_required != data_destruction_required;
		this.data_destruction_required = data_destruction_required;
	}
	
	public boolean isHardDrivePresent() {
		return hd_present;
	}
	
	public void setHardDrivePresent(boolean hd_present) {
		this.changed |= this.hd_present != hd_present;
		this.hd_present = hd_present;
	}
	
	public boolean isHardDriveWiped() {
		return hd_wiped;
	}
	
	public void setHardDriveWiped(boolean hd_wiped) {
		this.changed |= this.hd_wiped != hd_wiped;
		this.hd_wiped = hd_wiped;
	}
	
	public boolean isHardDriveRecycled() {
		return hd_recycled;
	}
	
	public void setHardDriveRecycled(boolean hd_recycled) {
		this.changed |= this.hd_recycled != hd_recycled;
		this.hd_recycled = hd_recycled;
	}
	
	@Override
	public boolean equals(Object other) {
		return other instanceof Item
				&& this.type.equals(((Item) other).type)
				&& this.serial_code.equalsIgnoreCase(((Item) other).serial_code);
	}
	
	@Override
	public int hashCode() {
		return hash_code;
	}
	
	public enum Type {
		COMPUTER('C'),
		LAPTOP('L'),
		MONITOR('M'),
		PRINTER('P'),
		SERVER('S'),
		HARD_DRIVE('H'),
		OTHER('O');
		
		private final char identifier;
		
		Type(char identifier) {
			this.identifier = Character.toUpperCase(identifier);
		}
		
		public char getIdentifier() {
			return identifier;
		}
		
		@Override
		public String toString() {
			return String.valueOf(identifier);
		}
		
		public static Type getType(String identifier) {
			if (identifier == null || identifier.isEmpty()) {
				return null;
			}
			String lower = identifier.toLowerCase();
			for (Type type : values()) {
				if (type.name().toLowerCase().equals(lower)) {
					return type;
				}
			}
			for (Type type : values()) {
				if (Character.toLowerCase(type.identifier) == lower.charAt(0)) {
					return type;
				}
			}
			return null;
		}
		
		public static Type getType(char identifier) {
			char lower = Character.toLowerCase(identifier);
			for (Type type : values()) {
				if (lower == Character.toLowerCase(type.identifier)) {
					return type;
				}
			}
			return null;
		}
	}
	
	@Override
	public void clearId(){
		super.clearId();
		this.changed = true;
	}
}
