package com.calebmilligan.codeprocessor.struct;

import com.calebmilligan.codeprocessor.util.Utils;
import com.calebmilligan.codeprocessor.util.cache.CacheMap;
import com.calebmilligan.codeprocessor.util.cache.ExpirationPolicy;
import com.calebmilligan.codeprocessor.util.cache.ExpiryType;
import com.calebmilligan.codeprocessor.util.logging.Loggers;

import java.sql.*;
import java.text.ParseException;
import java.util.Date;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Shipment extends DatabaseEntry {
	private static final CacheMap<Integer, Shipment> cache = new CacheMap<>(1, TimeUnit.MINUTES,
			ExpiryType.SINCE_ACCESSED, ExpirationPolicy.LAZY);
	private Date date;
	private String bol_number;
	private Customer customer;
	private Employee employee;
	private List<Item> items;
	private transient int hash_code;
	
	public static Shipment getShipment(int id, Connection connection) throws SQLException {
		if (cache.containsKey(id)) {
			return cache.get(id).getValue();
		}
		Object[] temp_storage = null;
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"date\", \"bol_number\", " +
				"\"customer_id\", \"employee_id\" FROM \"shipments\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					temp_storage = new Object[]{
							id,
							new Date(result.getDate(1).getTime()),
							result.getString(2),
							result.getInt(3),
							result.getInt(4)
					};
					
				}
			}
		}
		if (temp_storage != null) {
			List<Item> items = Item.getAllItems((Integer) temp_storage[0], connection);
			Shipment shipment = new Shipment(
					(Integer) temp_storage[0],
					(Date) temp_storage[1],
					(String) temp_storage[2],
					Customer.getCustomer((Integer) temp_storage[3], connection),
					Employee.getEmployee((Integer) temp_storage[4], connection),
					items
			);
			cache.cache(id, shipment);
			return shipment;
		}
		return null;
	}
	
	public static List<Shipment> getAllShipments(Connection connection) throws SQLException {
		List<Object[]> temp_storage = new LinkedList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"id\", \"date\", \"bol_number\", " +
				"\"customer_id\", \"employee_id\" FROM \"shipments\"")) {
			try (ResultSet result = statement.executeQuery()) {
				while (result.next()) {
					Object[] temp = new Object[]{
							result.getInt(1),
							null,
							result.getString(3),
							result.getInt(4),
							result.getInt(5)
					};
					try {
						temp[1] = Utils.ISO_DATE_FORMAT_SHORT.parse(result.getString(2));
					}
					catch (ParseException e) {
						Loggers.MAIN.warning("Error parsing date: " + result.getString(2));
						temp[1] = new Date();
					}
					temp_storage.add(temp);
				}
			}
		}
		List<Shipment> shipments = new ArrayList<>(temp_storage.size());
		for (Object[] temp_shipment : temp_storage) {
			List<Item> items = Item.getAllItems((Integer) temp_shipment[0], connection);
			Shipment shipment = new Shipment(
					(Integer) temp_shipment[0],
					(Date) temp_shipment[1],
					(String) temp_shipment[2],
					Customer.getCustomer((Integer) temp_shipment[3], connection),
					Employee.getEmployee((Integer) temp_shipment[4], connection),
					items
			);
			cache.cache(shipment.id, shipment);
			shipments.add(shipment);
		}
		return shipments;
	}
	
	public Shipment(Date date, String bol_number, Customer customer, Employee employee, List<Item> items) {
		super();
		this.date = date;
		this.bol_number = bol_number;
		this.customer = customer;
		this.employee = employee;
		this.items = items == null ? new LinkedList<>() : new LinkedList<>(items);
		calculateHash();
	}
	
	public Shipment(int id, Date date, String bol_number, Customer customer, Employee employee, List<Item> items) {
		super(id);
		this.date = date;
		this.bol_number = bol_number;
		this.customer = customer;
		this.employee = employee;
		this.items = items == null ? new LinkedList<>() : new LinkedList<>(items);
		calculateHash();
	}
	
	public Optional<Integer> getId() {
		return Optional.ofNullable(this.id);
	}
	
	@Override
	public void save(Connection connection) throws SQLException {
		if (id == null) {
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO \"shipments\" (\"date\", " +
					"\"bol_number\", \"customer_id\", \"employee_id\") VALUES (?, ?, ?, ?)")) {
				statement.setString(1, Utils.ISO_DATE_FORMAT_SHORT.format(date));
				statement.setString(2, bol_number);
				if (customer != null) {
					statement.setInt(3, customer.id);
				}
				else {
					statement.setNull(3, Types.INTEGER);
				}
				if (employee != null) {
					statement.setInt(4, employee.id);
				}
				else {
					statement.setNull(4, Types.INTEGER);
				}
				statement.execute();
			}
			try (PreparedStatement statement = connection.prepareStatement("SELECT last_insert_rowid()")) {
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						this.id = result.getInt(1);
					}
				}
			}
		}
		else {
			try (PreparedStatement statement = connection.prepareStatement("UPDATE \"shipments\" SET \"date\"=?, " +
					"\"bol_number\"=?, \"customer_id\"=?, \"employee_id\"=? WHERE \"id\"=?")) {
				statement.setString(1, Utils.ISO_DATE_FORMAT.format(date));
				statement.setString(2, bol_number);
				if (customer != null) {
					statement.setInt(3, customer.id);
				}
				else {
					statement.setNull(3, Types.INTEGER);
				}
				if (employee != null) {
					statement.setInt(4, employee.id);
				}
				else {
					statement.setNull(4, Types.INTEGER);
				}
				statement.setInt(5, id);
				statement.execute();
			}
		}
		for (Item item : items) {
			item.setShipmentId(id);
			item.save(connection);
		}
	}
	
	@Override
	public void delete(Connection connection) throws SQLException {
		if (id == null) {
			return;
		}
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM \"shipments\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			statement.execute();
		}
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
		calculateHash();
	}
	
	public Optional<String> getBoLNumber() {
		return Optional.ofNullable(bol_number);
	}
	
	public void setBolNumber(String bol_number) {
		this.bol_number = bol_number;
		calculateHash();
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
		calculateHash();
	}
	
	public Employee getEmployee() {
		return employee;
	}
	
	public void setEmployee(Employee employee) {
		this.employee = employee;
		calculateHash();
	}
	
	public List<Item> getItems() {
		return items == null ? (items = new LinkedList<>()) : items;
	}
	
	private void calculateHash() {
		this.hash_code = Objects.hash(customer, employee, date, bol_number);
	}
	
	@Override
	public int hashCode() {
		return hash_code;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Shipment &&
				Objects.equals(((Shipment) o).customer, customer) &&
				Objects.equals(((Shipment) o).employee, employee) &&
				Objects.equals(((Shipment) o).date, date) &&
				Objects.equals(((Shipment) o).bol_number, bol_number);
	}
	
	@Override
	public void clearId() {
		super.clearId();
		for (Item item : items) {
			item.clearId();
		}
	}
}
