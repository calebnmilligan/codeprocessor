package com.calebmilligan.codeprocessor.struct;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Caleb Milligan
 * Created on 3/5/2019.
 */
public abstract class DatabaseEntry {
	protected Integer id;
	
	public DatabaseEntry() {
	
	}
	
	public DatabaseEntry(int id) {
		this.id = id;
	}
	
	public Optional<Integer> getId() {
		return Optional.ofNullable(id);
	}
	
	public abstract void save(Connection connection) throws SQLException;
	
	public abstract void delete(Connection connection) throws SQLException;
	
	@Override
	public boolean equals(Object o) {
		return o instanceof DatabaseEntry && getClass().isInstance(o) && Objects.equals(((DatabaseEntry) o).id, this.id);
	}
	
	@Override
	public int hashCode() {
		return id == null ? 0 : id;
	}
	
	public void clearId() {
		this.id = null;
	}
}
