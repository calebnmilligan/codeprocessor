package com.calebmilligan.codeprocessor.struct;

import com.calebmilligan.codeprocessor.util.cache.CacheMap;
import com.calebmilligan.codeprocessor.util.cache.ExpirationPolicy;
import com.calebmilligan.codeprocessor.util.cache.ExpiryType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Employee extends DatabaseEntry {
	private static final CacheMap<Integer, Employee> cache = new CacheMap<>(1, TimeUnit.MINUTES,
			ExpiryType.SINCE_ACCESSED, ExpirationPolicy.LAZY);
	private String first_name;
	private String last_name;
	private transient int hash_code;
	
	public static Employee getEmployee(int id, Connection connection) throws SQLException {
		if (cache.containsKey(id)) {
			return cache.get(id).getValue();
		}
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"first_name\", \"last_name\" FROM " +
				"\"employees\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					Employee employee = new Employee(
							id,
							result.getString(1),
							result.getString(2)
					);
					cache.cache(id, employee);
					return employee;
				}
			}
		}
		return null;
	}
	
	public static List<Employee> getAllEmployees(Connection connection) throws SQLException {
		List<Employee> employees = new LinkedList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"id\", \"first_name\", \"last_name\" " +
				"FROM \"employees\"")) {
			try (ResultSet result = statement.executeQuery()) {
				while (result.next()) {
					Employee employee = new Employee(
							result.getInt(1),
							result.getString(2),
							result.getString(3)
					);
					cache.cache(employee.id, employee);
					employees.add(employee);
				}
			}
		}
		return new ArrayList<>(employees);
	}
	
	public Employee(int id, String first_name, String last_name) {
		super(id);
		this.first_name = first_name;
		this.last_name = last_name;
		calculateHash();
	}
	
	public Employee(String first_name, String last_name) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		calculateHash();
	}
	
	@Override
	public void save(Connection connection) throws SQLException {
		if (id == null) {
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO \"employees\" (\"first_name\", " +
					"\"last_name\") VALUES (?, ?)")) {
				statement.setString(1, first_name);
				statement.setString(2, last_name);
				statement.execute();
			}
			try (PreparedStatement statement = connection.prepareStatement("SELECT last_insert_rowid()")) {
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						this.id = result.getInt(1);
					}
				}
			}
		}
		else {
			try (PreparedStatement statement = connection.prepareStatement("UPDATE \"employees\" SET \"first_name\"=?, " +
					"\"last_name\"=? WHERE `id`=?")) {
				statement.setString(1, first_name);
				statement.setString(2, last_name);
				statement.setInt(3, id);
				statement.execute();
			}
		}
	}
	
	@Override
	public void delete(Connection connection) throws SQLException {
		if (id == null) {
			return;
		}
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM \"employees\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			statement.execute();
		}
	}
	
	public void setFirstName(String first_name) {
		this.first_name = first_name;
		calculateHash();
	}
	
	public String getFirstName() {
		return first_name;
	}
	
	public void setLastName(String last_name) {
		this.last_name = last_name;
		calculateHash();
	}
	
	public String getLastName() {
		return last_name;
	}
	
	public String getFormattedName() {
		String name = "<unknown>";
		if (last_name != null) {
			name = last_name;
		}
		if (first_name != null) {
			if (last_name != null) {
				name += ", ";
			}
			else {
				name = "";
			}
			name += first_name;
		}
		return name;
	}
	
	private void calculateHash() {
		String f_l = first_name == null ? null : first_name.toLowerCase();
		String l_l = last_name == null ? null : last_name.toLowerCase();
		this.hash_code = Objects.hash(f_l, l_l);
	}
	
	@Override
	public String toString() {
		return getFormattedName();
	}
	
	@Override
	public int hashCode() {
		return hash_code;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Employee &&
				(Objects.equals(((Employee) o).first_name, first_name) ||
						((Employee) o).first_name.equalsIgnoreCase(first_name)) &&
				(Objects.equals(((Employee) o).last_name, last_name) ||
						((Employee) o).last_name.equalsIgnoreCase(last_name));
	}
}
