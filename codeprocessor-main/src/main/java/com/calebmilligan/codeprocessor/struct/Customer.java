package com.calebmilligan.codeprocessor.struct;

import com.calebmilligan.codeprocessor.util.cache.CacheMap;
import com.calebmilligan.codeprocessor.util.cache.ExpirationPolicy;
import com.calebmilligan.codeprocessor.util.cache.ExpiryType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Customer extends DatabaseEntry {
	private static final CacheMap<Integer, Customer> cache = new CacheMap<>(1, TimeUnit.MINUTES,
			ExpiryType.SINCE_ACCESSED, ExpirationPolicy.LAZY);
	private String name;
	private String correspondent;
	private String contact_info;
	private String address_line_1;
	private String address_line_2;
	private String address_line_3;
	private transient int hash_code;
	
	public static Customer getCustomer(int id, Connection connection) throws SQLException {
		if (cache.containsKey(id)) {
			return cache.get(id).getValue();
		}
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"name\", \"correspondent\", \"contact_info\", " +
				"\"address_line_1\", \"address_line_2\", \"address_line_3\" FROM \"customers\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					Customer customer = new Customer(
							id,
							result.getString(1),
							result.getString(2),
							result.getString(3),
							result.getString(4),
							result.getString(5),
							result.getString(6)
					);
					cache.cache(id, customer);
					return customer;
				}
			}
		}
		return null;
	}
	
	public static List<Customer> getAllCustomers(Connection connection) throws SQLException {
		List<Customer> customers = new LinkedList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"id\", \"name\", \"correspondent\", " +
				"\"contact_info\", \"address_line_1\", \"address_line_2\", \"address_line_3\" FROM \"customers\"")) {
			try (ResultSet result = statement.executeQuery()) {
				while (result.next()) {
					Customer customer = new Customer(
							result.getInt(1),
							result.getString(2),
							result.getString(3),
							result.getString(4),
							result.getString(5),
							result.getString(6),
							result.getString(7)
					);
					customers.add(customer);
					cache.cache(customer.id, customer);
				}
			}
		}
		return new ArrayList<>(customers);
	}
	
	public Customer(int id, String name, String correspondent, String contact_info, String address_line_1,
					String address_line_2, String address_line_3) {
		super(id);
		this.name = name;
		this.correspondent = correspondent;
		this.contact_info = contact_info;
		this.address_line_1 = address_line_1;
		this.address_line_2 = address_line_2;
		this.address_line_3 = address_line_3;
		calculateHash();
	}
	
	public Customer(String name, String correspondent, String contact_info, String address_line_1, String address_line_2,
					String address_line_3) {
		super();
		this.name = name;
		this.correspondent = correspondent;
		this.contact_info = contact_info;
		this.address_line_1 = address_line_1;
		this.address_line_2 = address_line_2;
		this.address_line_3 = address_line_3;
		calculateHash();
	}
	
	@Override
	public void save(Connection connection) throws SQLException {
		if (id == null) {
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO \"customers\" (\"name\", " +
					"\"correspondent\", \"contact_info\", \"address_line_1\", \"address_line_2\", \"address_line_3\") " +
					"VALUES (?, ?, ?, ?, ?, ?)")) {
				statement.setString(1, name);
				statement.setString(2, correspondent);
				statement.setString(3, contact_info);
				statement.setString(4, address_line_1);
				statement.setString(5, address_line_2);
				statement.setString(6, address_line_3);
				statement.execute();
			}
			try (PreparedStatement statement = connection.prepareStatement("SELECT last_insert_rowid()")) {
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						this.id = result.getInt(1);
					}
				}
			}
		}
		else {
			try (PreparedStatement statement = connection.prepareStatement("UPDATE \"customers\" SET \"name\"=?, " +
					"\"correspondent\"=?, \"contact_info\"=?, \"address_line_1\"=?, \"address_line_2\"=?, " +
					"\"address_line_3\"=? WHERE \"id\"=?")) {
				statement.setString(1, name);
				statement.setString(2, correspondent);
				statement.setString(3, contact_info);
				statement.setString(4, address_line_1);
				statement.setString(5, address_line_2);
				statement.setString(6, address_line_3);
				statement.setInt(7, id);
				statement.execute();
			}
		}
	}
	
	@Override
	public void delete(Connection connection) throws SQLException {
		if (id == null) {
			return;
		}
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM \"customers\" WHERE \"id\"=?")) {
			statement.setInt(1, id);
			statement.execute();
		}
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
		calculateHash();
	}
	
	public String getCorrespondent() {
		return correspondent;
	}
	
	public void setCorrespondent(String correspondent) {
		this.correspondent = correspondent;
	}
	
	public String getContactInformation() {
		return contact_info;
	}
	
	public void setContactInformation(String contact_info) {
		this.contact_info = contact_info;
	}
	
	public String getAddressLine1() {
		return address_line_1;
	}
	
	public void setAddressLine1(String address_line_1) {
		this.address_line_1 = address_line_1;
	}
	
	public String getAddressLine2() {
		return address_line_2;
	}
	
	public void setAddressLine2(String address_line_2) {
		this.address_line_2 = address_line_2;
	}
	
	public String getAddressLine3() {
		return address_line_3;
	}
	
	public void setAddressLine3(String address_line_3) {
		this.address_line_3 = address_line_3;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	private void calculateHash() {
		this.hash_code = Objects.hash(name);
	}
	
	@Override
	public int hashCode() {
		return hash_code;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Customer &&
				(Objects.equals(((Customer) o).name, name) || ((Customer) o).name.equalsIgnoreCase(name));
	}
}
