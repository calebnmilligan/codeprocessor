package com.calebmilligan.codeprocessor.io;

import com.calebmilligan.codeprocessor.util.logging.Loggers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Caleb Milligan
 * Created on 2/23/2019.
 */
public class IOLoader {
	
	@SuppressWarnings("unchecked")
	public static Set<Class<Code10IO>> loadOutputClasses(File container) {
		if (!container.isDirectory()) {
			throw new IllegalArgumentException("File is not a directory");
		}
		Loggers.OUTPUT.info("Scanning for io formats in " + container.getAbsolutePath());
		Set<URL> urls = new HashSet<>();
		Set<JarFile> jar_files = new HashSet<>();
		File[] files = container.listFiles();
		if (files == null) {
			return new HashSet<>();
		}
		for (File file : files) {
			try {
				jar_files.add(new JarFile(file));
				urls.add(new URL("jar:file:" + file.getAbsolutePath() + "!/"));
			}
			catch (IOException e) {
				Loggers.OUTPUT.warning("Unable to open " + file.getName());
			}
		}
		URL[] url_array = new URL[urls.size()];
		urls.toArray(url_array);
		URLClassLoader cl = URLClassLoader.newInstance(url_array);
		Set<Class<Code10IO>> output_classes = new HashSet<>();
		for (JarFile file : jar_files) {
			Loggers.OUTPUT.info("Scanning " + file.getName() + " for suitable io formats");
			Enumeration<JarEntry> entries = file.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (entry.isDirectory() || !entry.getName().endsWith(".class")) {
					continue;
				}
				try {
					String name = entry.getName();
					name = name.substring(0, name.length() - ".class".length()).replace('/', '.');
					Class<?> loaded_class = cl.loadClass(name);
					if (Code10IO.class.isAssignableFrom(loaded_class)) {
						Loggers.OUTPUT.info("Found io class: " + loaded_class.getCanonicalName());
						output_classes.add((Class<Code10IO>) loaded_class);
					}
				}
				catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			try {
				file.close();
			}
			catch (IOException e) {
				Loggers.OUTPUT.warning("Unable to close " + file.getName() + ": " + e.getMessage());
			}
		}
		return output_classes;
	}
}
