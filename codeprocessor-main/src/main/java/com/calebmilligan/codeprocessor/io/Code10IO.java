package com.calebmilligan.codeprocessor.io;

import com.calebmilligan.codeprocessor.struct.Shipment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Caleb Milligan
 * Created on 2/22/2019.
 */
public abstract class Code10IO {
	public abstract void write(Shipment code10, OutputStream out) throws IOException;
	
	public abstract Shipment read(InputStream in) throws IOException;
}
