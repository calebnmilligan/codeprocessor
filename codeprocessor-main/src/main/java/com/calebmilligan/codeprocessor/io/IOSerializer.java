package com.calebmilligan.codeprocessor.io;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Caleb Milligan
 * Created on 2/23/2019.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface IOSerializer {
	String value();
	
	String fileType() default "";
}
