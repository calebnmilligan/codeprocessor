package com.calebmilligan.codeprocessor;

import com.calebmilligan.codeprocessor.struct.Customer;
import com.calebmilligan.codeprocessor.struct.Employee;
import com.calebmilligan.codeprocessor.struct.Item;
import com.calebmilligan.codeprocessor.struct.Shipment;
import com.calebmilligan.codeprocessor.util.ScannerInput;
import com.calebmilligan.codeprocessor.util.Utils;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

import javax.imageio.ImageIO;
import javax.sql.DataSource;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class Context {
	private final List<Image> icons = new ArrayList<>(6);
	private final Path base_dir;
	private SQLiteDataSource source;
	private final String database_file_name = "storage";
	private final Path database_file_path;
	private final Config config = new Config();
	private final ScannerInput input;
	
	public Context() throws Exception {
		this(new File("code10"));
	}
	
	public Context(File base_dir) throws Exception {
		if (base_dir.exists() && !base_dir.isDirectory()) {
			throw new IllegalArgumentException("Base dir is not a directory");
		}
		this.base_dir = base_dir.toPath();
		base_dir.mkdirs();
		database_file_path = this.base_dir.resolve(database_file_name + ".db");
		
		
		createDatabaseFileBackup();
		
		input = new ScannerInput(this);
		
		try {
			loadIcons();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		initDataSource();
		
		applySchema(source);
	}
	
	public Path getBasePath() {
		return base_dir;
	}
	
	public Path getDatabaseFilePath() {
		return database_file_path;
	}
	
	public void initDataSource() throws IOException{
		File database_file = database_file_path.toFile();
		if (!database_file.exists()) {
			database_file.createNewFile();
		}
		this.source = initDataSource(database_file_path.toFile());
	}
	
	private SQLiteDataSource initDataSource(File file) {
		SQLiteConfig config = new SQLiteConfig();
		config.enforceForeignKeys(true);
		config.setSynchronous(SQLiteConfig.SynchronousMode.FULL);
		SQLiteDataSource source = new SQLiteDataSource(config);
		String url = "jdbc:sqlite:" + file.getAbsolutePath();
		source.setUrl(url);
		return source;
	}
	
	private void applySchema(DataSource source) throws SQLException, IOException {
		try (Connection connection = source.getConnection()) {
			StringBuilder builder = new StringBuilder();
			byte[] buf = new byte[512];
			int read;
			InputStream in = Context.class.getResourceAsStream("/schema_blank.sql");
			while ((read = in.read(buf)) > 0) {
				builder.append(new String(buf, 0, read));
			}
			String[] queries = builder.toString().split(";");
			connection.setAutoCommit(false);
			for (String query : queries) {
				try (PreparedStatement statement = connection.prepareStatement(query)) {
					statement.execute();
				}
			}
			connection.commit();
		}
	}
	
	private void loadIcons() throws IOException {
		try (InputStream in = Context.class.getResourceAsStream("/icon_256_256.png")) {
			BufferedImage icon_256_256 = ImageIO.read(in);
			BufferedImage icon_256_256_no_alpha = new BufferedImage(256, 256, BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D g = icon_256_256_no_alpha.createGraphics();
			g.drawImage(icon_256_256, 0, 0, null);
			g.dispose();
			icon_256_256 = icon_256_256_no_alpha;
			Image icon_128_128 = icon_256_256.getScaledInstance(128, 128, BufferedImage.SCALE_SMOOTH);
			Image icon_96_96 = icon_256_256.getScaledInstance(96, 96, BufferedImage.SCALE_SMOOTH);
			Image icon_64_64 = icon_256_256.getScaledInstance(64, 64, BufferedImage.SCALE_SMOOTH);
			Image icon_48_48 = icon_256_256.getScaledInstance(48, 48, Image.SCALE_SMOOTH);
			Image icon_32_32 = icon_256_256.getScaledInstance(32, 32, Image.SCALE_SMOOTH);
			Image icon_16_16 = icon_256_256.getScaledInstance(16, 16, Image.SCALE_SMOOTH);
			icons.clear();
			icons.add(icon_16_16);
			icons.add(icon_32_32);
			icons.add(icon_48_48);
			icons.add(icon_64_64);
			icons.add(icon_96_96);
			icons.add(icon_128_128);
			icons.add(icon_256_256);
		}
	}
	
	public void createDatabaseFileBackup() throws IOException {
		File source_file = database_file_path.toFile();
		if (!source_file.exists()) {
			return;
		}
		
		Path base_path = base_dir.resolve("backups");
		NumberFormat iteration_format = new DecimalFormat("00");
		Date now = new Date();
		String date_string = Utils.ISO_DATE_FORMAT_SHORT.format(now);
		base_path = base_path.resolve(date_string);
		base_path.toFile().mkdirs();
		int iterations = 0;
		String iteration_string;
		File backup_file;
		while (true) {
			iteration_string = iteration_format.format(iterations);
			backup_file = base_path.resolve(database_file_name + "_" + date_string + "_" + iteration_string + ".db")
					.toFile();
			if (!backup_file.exists()) {
				break;
			}
			iterations++;
		}
		backup_file.getParentFile().mkdirs();
		backup_file.createNewFile();
		try (InputStream in = new FileInputStream(source_file);
			 OutputStream out = new FileOutputStream(backup_file)) {
			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) > 0) {
				out.write(buffer, 0, read);
			}
			out.flush();
		}
	}
	
	public Config getConfig() {
		return config;
	}
	
	public List<Image> getIcons() {
		return icons;
	}
	
	public ScannerInput getInput() {
		return input;
	}
	
	public void mergeDatabases(File output_file, File[] files, Logger logger) throws SQLException, IOException {
		final List<Employee> employees = new LinkedList<>();
		final List<Customer> customers = new LinkedList<>();
		final List<Shipment> shipments = new LinkedList<>();
		if(!output_file.isFile()){
			throw new IllegalArgumentException("Target is not a file");
		}
		if(output_file.exists() && output_file.length() != 0){
			throw new IllegalArgumentException("File exists");
		}
		if (logger != null) {
			logger.info("Reading all files");
		}
		for (File file : files) {
			if (logger != null) {
				logger.info("Reading data from " + file.getAbsolutePath());
			}
			SQLiteDataSource source = initDataSource(file);
			source.setReadOnly(true);
			try (Connection connection = source.getConnection()) {
				if (logger != null) {
					logger.info("Loading employee list");
				}
				employees.addAll(Employee.getAllEmployees(connection));
				if (logger != null) {
					logger.info("Loading customer list");
				}
				customers.addAll(Customer.getAllCustomers(connection));
				if (logger != null) {
					logger.info("Loading shipments");
				}
				shipments.addAll(Shipment.getAllShipments(connection));
			}
		}
		if (logger != null) {
			logger.info("Done");
			logger.info("Merging data");
		}
		final Set<Employee> final_employees = new HashSet<>(employees);
		final Set<Customer> final_customers = new HashSet<>(customers.size());
		final Set<Shipment> final_shipments = new HashSet<>(shipments.size());
		
		for (Customer customer : customers) {
			if (!final_customers.add(customer)) {
				for (Customer merge_customer : final_customers) {
					if (merge_customer.equals(customer)) {
						if (merge_customer.getCorrespondent() == null || merge_customer.getCorrespondent().trim().isEmpty()) {
							merge_customer.setCorrespondent(customer.getCorrespondent());
						}
						if (merge_customer.getContactInformation() == null || merge_customer.getContactInformation().trim().isEmpty()) {
							merge_customer.setContactInformation(customer.getContactInformation());
						}
						if (merge_customer.getAddressLine1() == null || merge_customer.getAddressLine1().trim().isEmpty()) {
							merge_customer.setAddressLine1(customer.getAddressLine1());
						}
						if (merge_customer.getAddressLine2() == null || merge_customer.getAddressLine2().trim().isEmpty()) {
							merge_customer.setAddressLine2(customer.getAddressLine2());
						}
						if (merge_customer.getAddressLine3() == null || merge_customer.getAddressLine3().trim().isEmpty()) {
							merge_customer.setAddressLine3(customer.getAddressLine3());
						}
						break;
					}
				}
			}
		}
		for (Shipment shipment : shipments) {
			if (!final_shipments.add(shipment)) {
				for (Shipment merge_shipment : final_shipments) {
					if (merge_shipment.equals(shipment)) {
						if (merge_shipment.getBoLNumber().orElse("").trim().isEmpty()) {
							merge_shipment.setBolNumber(shipment.getBoLNumber().orElse(null));
						}
						Set<Item> final_items = new HashSet<>(merge_shipment.getItems());
						final_items.addAll(shipment.getItems());
						merge_shipment.getItems().clear();
						merge_shipment.getItems().addAll(final_items);
						shipment.getItems().clear();
						shipment.getItems().addAll(final_items);
						break;
					}
				}
			}
		}
		if (logger != null) {
			logger.info("Done");
			logger.info("Writing data to merged file");
		}
		SQLiteDataSource source = initDataSource(output_file);
		applySchema(source);
		try (Connection connection = source.getConnection()) {
			for (Employee employee : final_employees) {
				employee.clearId();
				employee.save(connection);
			}
			for (Customer customer : final_customers) {
				customer.clearId();
				customer.save(connection);
			}
			for (Shipment shipment : final_shipments) {
				shipment.clearId();
				shipment.save(connection);
			}
		}
		if (logger != null) {
			logger.info("Done");
		}
	}
	
	private AtomicInteger connection_count = new AtomicInteger(0);
	
	class WrappedConnection implements Connection {
		private final Connection wrapped;
		
		public WrappedConnection(Connection wrapped) {
			this.wrapped = wrapped;
		}
		
		@Override
		public Statement createStatement() throws SQLException {
			return wrapped.createStatement();
		}
		
		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			return wrapped.prepareStatement(sql);
		}
		
		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return wrapped.prepareCall(sql);
		}
		
		@Override
		public String nativeSQL(String sql) throws SQLException {
			return wrapped.nativeSQL(sql);
		}
		
		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			wrapped.setAutoCommit(autoCommit);
		}
		
		@Override
		public boolean getAutoCommit() throws SQLException {
			return wrapped.getAutoCommit();
		}
		
		@Override
		public void commit() throws SQLException {
			wrapped.commit();
			;
		}
		
		@Override
		public void rollback() throws SQLException {
			wrapped.rollback();
		}
		
		@Override
		public void close() throws SQLException {
			wrapped.close();
			connection_count.decrementAndGet();
		}
		
		@Override
		public boolean isClosed() throws SQLException {
			return wrapped.isClosed();
		}
		
		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return wrapped.getMetaData();
		}
		
		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			wrapped.setReadOnly(readOnly);
		}
		
		@Override
		public boolean isReadOnly() throws SQLException {
			return wrapped.isReadOnly();
		}
		
		@Override
		public void setCatalog(String catalog) throws SQLException {
			wrapped.setCatalog(catalog);
		}
		
		@Override
		public String getCatalog() throws SQLException {
			return wrapped.getCatalog();
		}
		
		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			wrapped.setTransactionIsolation(level);
		}
		
		@Override
		public int getTransactionIsolation() throws SQLException {
			return wrapped.getTransactionIsolation();
		}
		
		@Override
		public SQLWarning getWarnings() throws SQLException {
			return wrapped.getWarnings();
		}
		
		@Override
		public void clearWarnings() throws SQLException {
			wrapped.clearWarnings();
			;
		}
		
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			return wrapped.createStatement(resultSetType, resultSetConcurrency);
		}
		
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return wrapped.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}
		
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return wrapped.prepareCall(sql, resultSetType, resultSetConcurrency);
		}
		
		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return wrapped.getTypeMap();
		}
		
		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			wrapped.setTypeMap(map);
		}
		
		@Override
		public void setHoldability(int holdability) throws SQLException {
			wrapped.setHoldability(holdability);
		}
		
		@Override
		public int getHoldability() throws SQLException {
			return wrapped.getHoldability();
		}
		
		@Override
		public Savepoint setSavepoint() throws SQLException {
			return wrapped.setSavepoint();
		}
		
		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return wrapped.setSavepoint(name);
		}
		
		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			wrapped.rollback(savepoint);
		}
		
		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			wrapped.releaseSavepoint(savepoint);
		}
		
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return wrapped.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		}
		
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return wrapped.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}
		
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return wrapped.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}
		
		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			return wrapped.prepareStatement(sql, autoGeneratedKeys);
		}
		
		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			return wrapped.prepareStatement(sql, columnIndexes);
		}
		
		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			return wrapped.prepareStatement(sql, columnNames);
		}
		
		@Override
		public Clob createClob() throws SQLException {
			return wrapped.createClob();
		}
		
		@Override
		public Blob createBlob() throws SQLException {
			return wrapped.createBlob();
		}
		
		@Override
		public NClob createNClob() throws SQLException {
			return wrapped.createNClob();
		}
		
		@Override
		public SQLXML createSQLXML() throws SQLException {
			return wrapped.createSQLXML();
		}
		
		@Override
		public boolean isValid(int timeout) throws SQLException {
			return wrapped.isValid(timeout);
		}
		
		@Override
		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			wrapped.setClientInfo(name, value);
		}
		
		@Override
		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			wrapped.setClientInfo(properties);
		}
		
		@Override
		public String getClientInfo(String name) throws SQLException {
			return wrapped.getClientInfo(name);
		}
		
		@Override
		public Properties getClientInfo() throws SQLException {
			return wrapped.getClientInfo();
		}
		
		@Override
		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return wrapped.createArrayOf(typeName, elements);
		}
		
		@Override
		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return wrapped.createStruct(typeName, attributes);
		}
		
		@Override
		public void setSchema(String schema) throws SQLException {
			wrapped.setSchema(schema);
		}
		
		@Override
		public String getSchema() throws SQLException {
			return wrapped.getSchema();
		}
		
		@Override
		public void abort(Executor executor) throws SQLException {
			wrapped.abort(executor);
		}
		
		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
			wrapped.setNetworkTimeout(executor, milliseconds);
		}
		
		@Override
		public int getNetworkTimeout() throws SQLException {
			return wrapped.getNetworkTimeout();
		}
		
		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return wrapped.unwrap(iface);
		}
		
		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return wrapped.isWrapperFor(iface);
		}
	}
	
	public Connection getConnection() throws SQLException {
		assert connection_count.getAndIncrement() == 0;
		return new WrappedConnection(source.getConnection());
	}
	
	public void dispose() {
	
	}
}
