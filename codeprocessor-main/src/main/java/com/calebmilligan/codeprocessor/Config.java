package com.calebmilligan.codeprocessor;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Config {
	private final Map<String, Object> entries = new HashMap<>();
	
	public void set(String path, String value) {
		entries.put(path, value);
	}
	
	public void set(String path, Long value) {
		entries.put(path, value);
	}
	
	public void set(String path, Double value) {
		entries.put(path, value);
	}
	
	public void set(String path, Integer value) {
		entries.put(path, value);
	}
	
	public void set(String path, Float value) {
		entries.put(path, value);
	}
	
	public void set(String path, Short value) {
		entries.put(path, value);
	}
	
	public void set(String path, Character value) {
		entries.put(path, value);
	}
	
	public void set(String path, Byte value) {
		entries.put(path, value);
	}
	
	public void set(String path, Boolean value) {
		entries.put(path, value);
	}
	
	public void set(String path, byte[] value) {
		entries.put(path, value);
	}
	
	public String getString(String path, String default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		return obj instanceof String ? (String) obj : String.valueOf(obj);
	}
	
	public Long getLong(String path, Long default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Long) {
			return (Long) obj;
		}
		if (obj instanceof Number) {
			return ((Number) obj).longValue();
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj);
			try {
				return Long.parseLong(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
			try {
				return (long) Double.parseDouble(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		throw new ClassCastException("Value is not numeric type");
	}
	
	public Double getDouble(String path, Double default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Double) {
			return (Double) obj;
		}
		if (obj instanceof Number) {
			return ((Number) obj).doubleValue();
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj);
			try {
				return Double.parseDouble(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		throw new ClassCastException("Value is not numeric type");
	}
	
	public Integer getInteger(String path, Integer default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Integer) {
			return (Integer) obj;
		}
		if (obj instanceof Number) {
			return ((Number) obj).intValue();
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj);
			try {
				return Integer.parseInt(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
			try {
				return (int) Double.parseDouble(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		throw new ClassCastException("Value is not numeric type");
	}
	
	public Float getFloat(String path, Float default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Float) {
			return (Float) obj;
		}
		if (obj instanceof Number) {
			return ((Number) obj).floatValue();
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj);
			try {
				return Float.parseFloat(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		throw new ClassCastException("Value is not numeric type");
	}
	
	public Short getShort(String path, Short default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Short) {
			return (Short) obj;
		}
		if (obj instanceof Number) {
			return ((Number) obj).shortValue();
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj);
			try {
				return Short.parseShort(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
			try {
				return (short) Double.parseDouble(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		throw new ClassCastException("Value is not numeric type");
	}
	
	public Character getCharacter(String path, Character default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Character) {
			return (Character) obj;
		}
		if (obj instanceof CharSequence) {
			CharSequence cs = (CharSequence) obj;
			if (cs.length() == 0) {
				return null;
			}
			return cs.charAt(0);
		}
		throw new ClassCastException("Value is not string type");
	}
	
	public Byte getByte(String path, Byte default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj == null) {
			return null;
		}
		if (obj instanceof Byte) {
			return (Byte) obj;
		}
		if (obj instanceof Number) {
			return ((Number) obj).byteValue();
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj);
			try {
				return Byte.parseByte(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
			try {
				return (byte) Double.parseDouble(str);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		throw new ClassCastException("Value is not numeric type");
	}
	
	public Boolean getBoolean(String path, Boolean default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj instanceof Boolean) {
			return (Boolean) obj;
		}
		if (obj instanceof Number) {
			return !(((Number) obj).doubleValue() == 0 || ((Number) obj).longValue() == 0);
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = String.valueOf(obj).toLowerCase();
			switch (str) {
				case "1":
				case "true":
				case "yes":
				case "y":
				case "t":
					return true;
				default:
					return false;
			}
		}
		return obj != null;
	}
	
	public byte[] getBytes(String path, byte[] default_value) {
		Object obj = entries.getOrDefault(path, default_value);
		if (obj instanceof byte[]) {
			return (byte[]) obj;
		}
		if (obj instanceof CharSequence || obj instanceof Character) {
			String str = obj.toString();
			return str.getBytes();
		}
		throw new ClassCastException("Value is not byte stream");
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(String path, T default_value) throws ClassCastException {
		return (T) entries.getOrDefault(path, default_value);
	}
	
	public boolean isSet(String path) {
		return entries.containsKey(path);
	}
	
	public void save(Connection connection) throws SQLException {
		List<Map.Entry<String, Object>> entry_list = new ArrayList<>(entries.entrySet());
		if (entry_list.isEmpty()) {
			return;
		}
		
		StringBuilder query_builder = new StringBuilder("REPLACE INTO \"configuration\" (\"key\", \"value\") VALUES ");
		for (int i = 0; i < entry_list.size(); i++) {
			query_builder.append("(?, ?), ");
		}
		query_builder.delete(query_builder.length() - 2, query_builder.length());
		try (PreparedStatement statement = connection.prepareStatement(query_builder.toString())) {
			for (int i = 0; i < entry_list.size(); i++) {
				Map.Entry<String, Object> entry = entry_list.get(i);
				statement.setString(i * 2 + 1, entry.getKey());
				Object value = entry.getValue();
				if (value instanceof byte[]) {
					Blob blob = connection.createBlob();
					blob.setBytes(0, (byte[]) value);
					statement.setBlob(i * 2 + 2, blob);
				}
				else {
					statement.setString(i * 2 + 2, value == null ? null : String.valueOf(value));
				}
			}
			statement.execute();
		}
	}
	
	public void load(Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT \"key\", \"value\" FROM \"configuration\"")) {
			try (ResultSet result = statement.executeQuery()) {
				Map<String, Object> values = new HashMap<>();
				while (result.next()) {
					values.put(result.getString(1), result.getObject(2));
				}
				entries.clear();
				entries.putAll(values);
			}
		}
	}
}