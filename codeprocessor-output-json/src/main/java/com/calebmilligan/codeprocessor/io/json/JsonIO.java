package com.calebmilligan.codeprocessor.io.json;

import com.calebmilligan.codeprocessor.Code10Context;
import com.calebmilligan.codeprocessor.struct.Item;
import com.calebmilligan.codeprocessor.io.Code10IO;
import com.calebmilligan.codeprocessor.io.IOSerializer;
import com.grack.nanojson.JsonAppendableWriter;
import com.grack.nanojson.JsonWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Caleb Milligan
 * Created on 2/23/2019.
 */
@IOSerializer(value = "JSON", fileType = ".json")
public class JsonIO extends Code10IO {

	@Override
	public Code10Context read(InputStream in) throws IOException {
		return null;
	}

	@Override
	public void write(Code10Context code10, OutputStream out) {
		JsonAppendableWriter writer = JsonWriter.on(out);
		writer
			.object()
				.object("meta")
					.value("date_or_bol", code10.getDateOrBOLNumber())
					.value("employee_name", code10.getEmployeeName())
					.value("customer_name", code10.getCustomerName())
					.value("customer_attn", code10.getCustomerAttn())
					.array("customer_address")
						.value(code10.getCustomerAddress()[0])
						.value(code10.getCustomerAddress()[1])
					.end()
				.end()
			.array("items");
		writer.array();
		for (Code10ItemTableRow row : code10.getItems()) {
			Item item = row.getItem();
			writer
				.object()
					.value("type", item.getType().getIdentifier())
					.value("serial_code", item.getSerialCode())
					.value("refurbished", item.isRefurbished())
					.value("recycled", item.isRecycled())
					.value("data_destruction_required", item.isDataDestructionRequired())
					.value("hard_drive_present", item.isHardDrivePresent())
					.value("hard_drive_wiped", item.isHardDriveWiped())
					.value("hard_drive_recycled", item.isHardDriveRecycled())
				.end();
		}
		writer.end()
			.end();
	}
}
